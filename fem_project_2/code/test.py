import numpy as np
# import Mesh as m
import LinearTriangle as lt

# x = np.array([1, 4, 5])
# y = np.array([3, 5, 7])
#
# LT_element = lt.LinearTriangle(x, y, np.array([1, 2, 3]))
#
# LT_element.display_nodes()
#
# g_nodes_x = np.array([0, 1.5, 2, 3, 3])
# g_nodes_y = np.array([1, 0, 1, 1, 2])
#
# ICA = np.array([
#     [1, 2, 3],
#     [3, 2, 4],
#     [3, 4, 5]
# ])
#
# my_mesh = m.Mesh(g_nodes_x, g_nodes_y, ICA)
# my_mesh.display_mesh("Test mesh")
#
# a = np.array([[1, 2], [3, 4]])
# b = np.array([[2], [3]])
# print(b*a)
#
# aTri = lt.LinearTriangle(np.array([0, 1, 0]),
#                   np.array([0, 0, 1]),
#                   [0, 1, 2],
#                   1)
# e = aTri
# e.el_F += 1
# e.el_F *= 3
# print(aTri.el_F)
#
# test_ar = np.arange(12).reshape([3,4])
# print(test_ar)
# print(test_ar[[0, 1], :])
# print(test_ar[[0, 1], :][0, 1])
# print(test_ar[:, [0, 2]][[0, 2], :])
# test_ar[[0, 2, 2, 0], [0, 2, 0, 2]] = 0
# print(test_ar)
# if 0:
#     print(True)
#
# print(np.array([[1, 2, 3], [4, 5, 6]]).T)
def som():
    return 1, 2, 3


print(4-(3**0.5))