"""
lin_tri_assignment.py
Coded by: Benjamin Alheit
Description: FEM assignment. Python file containing function to interpolate values through linear triangle.
"""
import numpy as np


def interpolate_T(x, y, T_1, T_2, T_3):
    """
    interpolate_T: Interpolates temperature values of a triangle with vertices (1,1), (1.5,0) and (2,1) to some point
    (x,y) in the domain of the triangle
    :param x: x position of interest
    :param y: y position of interest
    :param T_1: Temperature at (1,1)
    :param T_2: Temperature at (1.5,0)
    :param T_3: Temperature at (2,1)
    :return: Temperature at (x,y)
    """
    # Construct M matrix
    M = np.array([[1, 1, 1],
                  [1, 1.5, 2],
                  [1, 0, 1]])

    
    pos_vec = np.array([1, x, y]).T
    xi = np.matmul(np.linalg.inv(M), pos_vec)

    logic = np.sum(np.add(1 < xi, xi < 0))
    if logic:
        print("Position not in element")

    else:
        T = np.array([T_1, T_2, T_3]).T
        T_xy = np.dot(T, xi)
        return T_xy


print(interpolate_T(1.5, 0.5, 1, 2, 3))