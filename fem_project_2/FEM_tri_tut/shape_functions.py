import numpy as np


def linear_triangle_shape_functions(el_x, el_y, pos):
    M = np.array([[1, 1, 1],
                  el_x,
                  el_y])
    pos_vec = np.array(np.append(np.array(1), pos)).T
    xi = np.matmul(np.linalg.inv(M), pos_vec)
    return xi


some_el_x = np.array([0, 0, 1])
some_el_y = np.array([1, 0, 0])

print(linear_triangle_shape_functions(some_el_x, some_el_y, np.array([0, 0])))