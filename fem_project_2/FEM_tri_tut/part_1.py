import numpy as np
import matplotlib.pyplot as plt

g_nodes_x = np.array([0, 1.5, 2, 3, 3])
g_nodes_y = np.array([1, 0, 1, 1, 2])

ICA = np.array([
    [1, 2, 3],
    [3, 2, 4],
    [3, 4, 5]
])

mesh_plot = plt

for i in range(len(ICA)):
    el_nodes = ICA[i]-1
    el_x = g_nodes_x[el_nodes]
    el_y = g_nodes_y[el_nodes]
    cent_x = np.sum(el_x)/3
    cent_y = np.sum(el_y)/3
    mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]))
    if i == 2:
        dot_colour = 'b'
    else:
        dot_colour = 'r'
    mesh_plot.scatter(cent_x, cent_y, color=dot_colour)

mesh_plot.grid()
plt.show()