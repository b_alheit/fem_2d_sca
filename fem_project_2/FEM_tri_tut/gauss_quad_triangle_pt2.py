import numpy as np
import matplotlib.pyplot as plt

n_gp = 4

def func(x, y):
    # return 6
    return 1 + x + y
    # return (x - 4) ** 2 + (y - 1)**2


# g_nodes_x = np.array([1, 1.5, 2, 3, 3, 4])
# g_nodes_y = np.array([1, 0, 1, 1, 2, 1.5])
#
# ICA = np.array([
#     [1, 2, 3],
#     [3, 2, 4],
#     [3, 4, 5],
#     [1, 3, 5],
#     [4, 6, 5]
# ])

g_nodes_x = np.array([0, 4, 8, 8, 4, 0, 4])
g_nodes_y = np.array([-1, 1, -1, 3, 5, 3, -3])

ICA = np.array([
    [1, 2, 6],
    [6, 2, 5],
    [2, 4, 5],
    [2, 3, 4],
    [7, 3, 2],
    [1, 7, 2]])



n_nodes = np.max(ICA)
ICA -= 1
n_el = len(ICA)

if n_gp == 1:
    xi = np.array([1/3])
    eta = np.array([1/3])
    weights = np.array([1])
elif n_gp == 2:
    print("Cannot have 2 Gauss points. Three gauss points have been used instead")
    n_gp = 3
    xi = np.array([1/6, 2/3, 1/6])
    eta = np.array([1/6, 1/6, 2/3])
    weights = np.array([1/3, 1/3, 1/3])
elif n_gp == 3:
    xi = np.array([1/6, 2/3, 1/6])
    eta = np.array([1/6, 1/6, 2/3])
    weights = np.array([1/3, 1/3, 1/3])
elif n_gp == 4:
    xi = np.array([1/3, 1/5, 3/5, 1/5])
    eta = np.array([1/3, 1/5, 1/5, 3/5])
    weights = np.array([-9/16, 25/48, 25/48, 25/48])
elif n_gp > 4:
    print("Max gauss points available is 4. Hence 4 gauss points used instead.")
    n_gp = 4
    xi = np.array([1/3, 1/5, 3/5, 1/5])
    eta = np.array([1/3, 1/5, 1/5, 3/5])
    weights = np.array([-9/16, 25/48, 25/48, 25/48])
else:
    print("Invalid value chosen for number of Gauss points. 4 Gauss points used instead for accuracy")
    n_gp = 4
    xi = np.array([1 / 3, 1 / 5, 3 / 5, 1 / 5])
    eta = np.array([1 / 3, 1 / 5, 1 / 5, 3 / 5])
    weights = np.array([-9 / 16, 25 / 48, 25 / 48, 25 / 48])


def physical_to_iso(nodes_pos, xi, eta):
    interpolants = np.array([xi, eta, 1 - xi - eta]).T
    ans = np.dot(nodes_pos, interpolants)
    return ans


int_el = np.zeros(n_el)
F_global = np.zeros(n_nodes).T

for i in range(n_el):
    x_local = g_nodes_x[ICA[i, :]]
    y_local = g_nodes_y[ICA[i, :]]
    F_local = F_global.T[ICA[i, :]]
    M = np.array([[1, 1, 1],
                  x_local,
                  y_local]).T
    A_e = np.linalg.det(M) / 2
    for j in range(n_gp):
        N = np.array([xi[j], eta[j], 1 - xi[j] - eta[j]])
        F_global[ICA[i, :]] += A_e * N.T * weights[j] * func(physical_to_iso(x_local, xi[j], eta[j]), physical_to_iso(y_local, xi[j], eta[j]))

print("Global force ", F_global)

# mesh_plot = plt
# for i in range(n_el):
#     el_nodes = ICA[i]
#     el_x = g_nodes_x[el_nodes]
#     el_y = g_nodes_y[el_nodes]
#     mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]))
#
#
#
#
#
# mesh_plot.grid()
# plt.show()