import numpy as np
import matplotlib.pyplot as plt

g_nodes_x = np.array([1, 1.5, 2, 3, 3])
g_nodes_y = np.array([1, 0, 1, 1, 2])

ICA = np.array([
    [1, 2, 3],
    [3, 2, 4],
    [3, 4, 5]
])

mesh_plot = plt

for i in range(len(ICA)):
    el_nodes = ICA[i]-1
    el_x = g_nodes_x[el_nodes]
    el_y = g_nodes_y[el_nodes]
    mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]))

x_rand = np.random.rand(1, 1000) * 3 + 1
y_rand = np.random.rand(1, 1000) * 2

logic = 2 < x_rand
logic = np.multiply(np.multiply(np.multiply(logic, x_rand < 3), 1 < y_rand), y_rand < x_rand-1)
mesh_plot.scatter(x_rand, y_rand, c=logic, cmap='jet')


mesh_plot.grid()
plt.show()