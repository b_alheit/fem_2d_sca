import numpy as np
import Mesh
from LinearTriangle import LinearTriangle as lt


def D(x, y):
    # return 5
    return 5*np.array([[1, 0],
                      [0, 1]])


def S(x, y):
    return 0.5*((8-x)**2)*((4-y)**2)
    # return 0


def q_bar(x, y):
    if x == 0 and 0 <= y <= 4:
        q = -3*(y**2)
    else:
        q = 0
    return q
    # return 0


def T_bar(x, y):
    if y == 0:
        T = 0
    else:
        T = None
    return T


nodes_x = np.array([0, 4, 8, 0, 8, 7, 4, 8-(3**0.5), 0, 4, 6])
nodes_y = np.array([0, 0, 0, 1, 2, 4-(3**0.5), 3, 3, 4, 4, 4])
BC = np.zeros(len(nodes_x))
BC[0:3] = 1  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes

ICA = np.array([
    [1, 2, 4],
    [4, 2, 7],
    [4, 7, 9],
    [9, 7, 10],
    [2, 3, 6],
    [3, 5, 6],
    [2, 6, 8],
    [2, 8, 7],
    [7, 8, 10],
    [10, 8, 11]
])

tri_mesh = Mesh.Mesh(nodes_x, nodes_y, ICA, D, q_bar, T_bar, S, BC, lt)

tri_mesh.int_F_bound(2)
tri_mesh.int_K(1)
tri_mesh.int_F_bod(3)
tri_mesh.assemble()
tri_mesh.solve_d()
tri_mesh.plot_solution(200, 100, "Linear Triangle Solution", "cont")

tri_mesh.plot_AB(1000, "heat")
tri_mesh.plot_AB(1000, "temp")

