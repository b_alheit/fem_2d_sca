import numpy as np
import matplotlib.pyplot as plt

res = 100

def N_single(xi):
    return np.array([[0.5*(xi-1)*xi],
                     [(1-xi)*(1+xi)],
                     [0.5*xi*(1+xi)]])

def N(xi, eta):
    i = np.outer(N_single(eta), N_single(xi))
    return np.array([i[0, 0],
                     i[0, 2],
                     i[2, 2],
                     i[2, 0],
                     i[0, 1],
                     i[1, 2],
                     i[2, 1],
                     i[1, 0],
                     i[1, 1]])


def dN_single(xi):
    return np.array([[0.5*(2*xi-1)],
                     [-2*xi],
                     [0.5*(1+2*xi)]])


def GN(xi, eta):
    out = np.zeros([2, 9])

    dN_dXi = np.outer(N_single(eta), dN_single(xi))
    out[0, :] = np.array([dN_dXi[0, 0],
                          dN_dXi[0, 2],
                          dN_dXi[2, 2],
                          dN_dXi[2, 0],
                          dN_dXi[0, 1],
                          dN_dXi[1, 2],
                          dN_dXi[2, 1],
                          dN_dXi[1, 0],
                          dN_dXi[1, 1]])

    dN_dEta = np.outer(dN_single(eta), N_single(xi))
    out[1, :] = np.array([dN_dEta[0, 0],
                          dN_dEta[0, 2],
                          dN_dEta[2, 2],
                          dN_dEta[2, 0],
                          dN_dEta[0, 1],
                          dN_dEta[1, 2],
                          dN_dEta[2, 1],
                          dN_dEta[1, 0],
                          dN_dEta[1, 1]])
    return out


def J_mat(xi, eta, x, y):
    return np.matmul(GN(xi, eta), np.array([x, y]).T)


def J(xi, eta, x, y):
    return np.linalg.det(J_mat(xi, eta, x, y))

# x = np.array([-1, 1, 1, -1, 0, 1, 0, -1, 0])
# y = np.array([-1, -1, 1, 1, -1, 0, 1, 0, 0])


x = np.array([4,
              7,
              8-(3**0.5),
              4,
              (7+4)/2,
              8+2*np.cos(np.deg2rad(225)),
              (12-(3**0.5))/2,
              4])

y = np.array([0,
              4-(3**0.5),
              3,
              3,
              (4-(3**0.5))/2,
              4+2*np.sin(np.deg2rad(225)),
              3,
              1.5])

x = np.append(x, np.average(x))
y = np.append(y, np.average(y))


Xi = np.zeros([res, res])
Eta = np.zeros([res, res])
X = np.zeros([res, res])
Y = np.zeros([res, res])
Z = np.zeros([res, res])

xi = np.linspace(-1, 1, res)
eta = np.linspace(-1, 1, res)

for i in range(res):
    for j in range(res):
        Xi[i, j] = xi[i]
        Eta[i, j] = eta[j]
        X[i, j] = np.dot(N(xi[i], eta[j]), x)
        Y[i, j] = np.dot(N(xi[i], eta[j]), y)
        Z[i, j] = J(xi[i], eta[j], x, y)


plot = plt

# plot.pcolor(Xi, Eta, Z, cmap="jet")
# plot.xlabel(r"$\xi$")
# plot.ylabel(r"$\eta$")
# plot.title(r"Jacobian values  of element [4] in isoparametric space")
plot.pcolor(X, Y, Z, cmap="jet")
plot.xlabel(r"x (cm)")
plot.ylabel(r"y (cm)")
plot.title(r"Jacobian values  of element [4] in physical space")
plot.grid()
plot.colorbar()
plot.show()

