import matplotlib.pyplot as plt
from matplotlib import cm
import numpy as np
import math

class Mesh:
    def __init__(self, x, y, ICA, D, q_bar, T_bar, S, BCs, element):
        self.x = x
        self.y = y
        self.BCs = BCs
        self.n_nodes = len(x)
        self.ICA = ICA - 1
        self.n_el = len(ICA)
        self.K = np.zeros([self.n_nodes, self.n_nodes])
        self.d = np.zeros(self.n_nodes)
        self.F = np.zeros(self.n_nodes)
        self.F_bod = np.zeros(self.n_nodes)
        self.F_bound = np.zeros(self.n_nodes)
        self.q_bar = q_bar
        self.D = D
        self.T_bar = T_bar
        self.S = S
        # self.x_map =
        # self.y_map
        # self.d_map
        self.Elements = np.empty(self.n_el, dtype=element)
        for i in range(self.n_el):
            local_nodes = self.ICA[i]
            self.Elements[i] = element(x[local_nodes], y[local_nodes], local_nodes, i)

    def display_mesh(self, title):
        mesh_plot = plt
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, str(i+1), color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        for i in range(self.n_nodes):
            mesh_plot.text(self.x[i], self.y[i], str(i + 1), color='red', fontsize=10)

        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()

    def lin_quad_selector(self, n_gp):
        if n_gp == 1:
            xi = np.array([0])
            weights = np.array([2])
        elif n_gp == 2:
            xi = np.array([-1/(3**0.5), 1/(3**0.5)])
            weights = np.array([1, 1])
        elif n_gp == 3:
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        else:
            print("Invalid input for linear quadrature. Gauss points set to 3 for accuracy.")
            n_gp = 3
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        return n_gp, xi, weights

    # def line_map_to_physical(self, xi, x1, x2):
    #     return (x2 + x1)/2 + xi*(x2-x1)/2

    def int_K(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                x = e.iso_to_physical(e.x, xi, eta)
                y = e.iso_to_physical(e.y, xi, eta)
                return np.matmul(e.B(xi, eta).T, np.matmul(self.D(x, y), e.B(xi, eta)))

            e.el_K = e.gauss(n_gp, integrand)

    def int_F_bod(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                x = e.iso_to_physical(e.x, xi, eta)
                y = e.iso_to_physical(e.y, xi, eta)
                return e.N(xi, eta) * self.S(x, y)

            e.el_F_bod = e.gauss(n_gp, integrand)

    def int_F_bound(self, n_gp):
        n_gp, xi, weights = self.lin_quad_selector(n_gp)
        for i in range(self.n_el):
            e = self.Elements[i]

            for j in range(e.edges):
                end_index = (j+2) % e.edges-1
                x_edge = np.array([e.x[j], e.x[end_index]])
                y_edge = np.array([e.y[j], e.y[end_index]])
                J = np.sqrt((x_edge[-1] - x_edge[0]) ** 2 + (y_edge[-1] - y_edge[0]) ** 2) / 2

                for k in range(n_gp):
                    N = e.N_edge(j, xi[k])
                    x = np.dot(e.x, N)
                    y = np.dot(e.y, N)
                    e.el_F_bound += weights[k] * J * N * self.q_bar(x, y)

    def assemble(self):
        self.K = np.zeros([self.n_nodes, self.n_nodes])
        self.F = np.zeros(self.n_nodes)
        self.F_bod = np.zeros(self.n_nodes)
        self.F_bound = np.zeros(self.n_nodes)
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.n_nodes):
                self.K[e.g_nodes[j], e.g_nodes] += e.el_K[j, :]
            self.F_bod[e.g_nodes] += e.el_F_bod
            self.F_bound[e.g_nodes] += e.el_F_bound
        self.F = self.F_bod - self.F_bound

    def solve_d(self):
        K_cond = self.K
        F_cond = self.F
        for i in range(self.n_nodes):
            if self.BCs[i] == 1:
                F_cond -= self.T_bar(self.x[i], self.y[i]) * K_cond[:, i]
                K_cond[i, :] = 0
                K_cond[:, i] = 0
                K_cond[i, i] = 1
        for i in range(self.n_nodes):
            if self.BCs[i] == 1:
                F_cond[i] = self.T_bar(self.x[i], self.y[i])

        self.d = np.matmul(np.linalg.inv(K_cond), F_cond)
        for i in range(self.n_el):
            self.Elements[i].d = self.d[self.ICA[i]]

    def solution(self, x, y):
        for i in range(self.n_el):
            e = self.Elements[i]
            if e.in_element(x, y):
                return np.matmul(e.Nxy(x, y), e.d)
        # print("Position not in domain")
        return None

    def plot_solution(self, x_res, y_res, title, type):
        x = np.linspace(np.min(self.x), np.max(self.x), x_res)
        y = np.linspace(np.min(self.y), np.max(self.y), y_res)
        x, y = np.meshgrid(x, y)
        z = np.zeros([y_res, x_res])
        for i in range(x_res):
            for j in range(y_res):
                z[j, i] = self.solution(x[j, i], y[j, i])

        masked_array = np.ma.array(z, mask=np.isnan(z))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        if type == "smooth":
            c_plot = mesh_plot.pcolor(x, y, masked_array, cmap=cmap)
        elif type == "cont":
            c_plot = mesh_plot.contourf(x, y, masked_array, 10, cmap=cmap)
        else:
            print("Error. Invalid type chosen")
        mesh_plot.colorbar(c_plot)
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / 3
            cent_y = np.sum(el_y) / 3
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (cm)")
        mesh_plot.ylabel("y (cm)")
        mesh_plot.show()

    def plot(self, X, Y, Z):

        masked_array = np.ma.array(Z, mask=np.isnan(Z))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        c_plot = mesh_plot.contourf(X, Y, masked_array, cmap=cmap)
        mesh_plot.colorbar(c_plot)
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "["+str(i+1)+"]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title("FEM solution")
        mesh_plot.xlabel("x (m)")
        mesh_plot.ylabel("y (m)")
        mesh_plot.show()
    #
    # def plot_solution_element_wise(self, apprx_n_points):
    #     pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
    #     xi_pos = np.linspace(-1, 1, pp_iso)
    #     eta_pos = xi_pos
    #     Xs = np.zeros([self.n_el, pp_iso, pp_iso])
    #     Ys = np.zeros([self.n_el, pp_iso, pp_iso])
    #     Zs = np.zeros([self.n_el, pp_iso-1, pp_iso-1])
    #     for i in range(self.n_el):
    #         X = np.zeros([pp_iso, pp_iso])
    #         Y = np.zeros([pp_iso, pp_iso])
    #         Z = np.zeros([pp_iso-1, pp_iso-1])
    #         e = self.Elements[i]
    #         for j in range(pp_iso-1):
    #             for k in range(pp_iso-1):
    #                 X[j, k] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k])
    #                 X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
    #                 X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
    #                 X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])
    #
    #                 Y[j, k] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k])
    #                 Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
    #                 Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
    #                 Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])
    #
    #                 xi_av = (xi_pos[j] + xi_pos[j+1])/2
    #                 eta_av = (eta_pos[k] + eta_pos[k+1])/2
    #
    #                 Z[j, k] = np.matmul(e.N(xi_av, eta_av), e.d)
    #         Xs[i] = X
    #         Ys[i] = Y
    #         Zs[i] = Z
    #     masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
    #     cmap = cm.jet
    #     cmap.set_bad('white', 1.)
    #
    #     mesh_plot = plt
    #     min_val, max_val = np.min(Zs), np.max(Zs)
    #
    #     for i in range(self.n_el):
    #         mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
    #         # mesh_plot.contourf(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
    #     mesh_plot.colorbar()
    #     for i in range(len(self.ICA)):
    #         el_nodes = self.ICA[i]
    #         el_x = self.x[el_nodes]
    #         el_y = self.y[el_nodes]
    #         cent_x = np.sum(el_x) / len(el_x)
    #         cent_y = np.sum(el_y) / len(el_y)
    #         mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
    #         mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')
    #
    #     mesh_plot.scatter(self.x, self.y, color='black')
    #
    #     mesh_plot.xlim(np.min(self.x), np.max(self.x))
    #     mesh_plot.ylim(np.min(self.y), np.max(self.y))
    #     mesh_plot.grid()
    #     mesh_plot.title("FEM solution")
    #     mesh_plot.xlabel("x (m)")
    #     mesh_plot.ylabel("y (m)")
    #     mesh_plot.show()
    #

    def plot_solution_element_wise(self, apprx_n_points, title, type):
        pp_iso = int(1+math.ceil(math.sqrt(apprx_n_points/self.n_el)))
        xi_pos = np.linspace(-1, 1, pp_iso)
        eta_pos = xi_pos
        Xs = np.zeros([self.n_el, pp_iso, pp_iso])
        Ys = np.zeros([self.n_el, pp_iso, pp_iso])
        Zs = np.zeros([self.n_el, pp_iso, pp_iso])
        for i in range(self.n_el):
            X = np.zeros([pp_iso, pp_iso])
            Y = np.zeros([pp_iso, pp_iso])
            Z = np.zeros([pp_iso, pp_iso])
            e = self.Elements[i]
            for j in range(pp_iso):
                for k in range(pp_iso):
                    X[j, k] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k])
                    # X[j+1, k] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k])
                    # X[j+1, k+1] = e.iso_to_physical(e.x, xi_pos[j+1], eta_pos[k+1])
                    # X[j, k+1] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k+1])

                    Y[j, k] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k])
                    # Y[j + 1, k] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k])
                    # Y[j + 1, k + 1] = e.iso_to_physical(e.y, xi_pos[j + 1], eta_pos[k + 1])
                    # Y[j, k + 1] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k + 1])

                    # xi_av = (xi_pos[j] + xi_pos[j+1])/2
                    # eta_av = (eta_pos[k] + eta_pos[k+1])/2

                    Z[j, k] = np.matmul(e.N(xi_pos[j], eta_pos[k]), e.d)
            Xs[i] = X
            Ys[i] = Y
            Zs[i] = Z
        masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
        cmap = cm.jet
        cmap.set_bad('white', 1.)

        mesh_plot = plt
        min_val, max_val = np.min(Zs), np.max(Zs)

        for i in range(self.n_el):
            if type == "smooth":
                mesh_plot.pcolor(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap)
            elif type == "cont":
                mesh_plot.contourf(Xs[i], Ys[i], masked_array[i], np.linspace(min_val, np.around(max_val, -1), 15), vmin=min_val, vmax=max_val, cmap=cmap)
            else:
                print("Error. Invalid type chosen.")
        mesh_plot.colorbar()

        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, "[" + str(i + 1) + "]", color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        mesh_plot.xlim(np.min(self.x), np.max(self.x))
        mesh_plot.ylim(np.min(self.y), np.max(self.y))
        mesh_plot.grid()
        mesh_plot.title(title)
        mesh_plot.xlabel("x (cm)")
        mesh_plot.ylabel("y (cm)")
        mesh_plot.show()



    def plot_AB(self, res, type):
        x = np.linspace(np.min(self.x), 6, res)
        q = np.zeros([2, len(x)])
        T = np.zeros(len(x))
        for i in range(len(x)):
            for j in range(self.n_el):
                e = self.Elements[j]
                if e.in_element(x[i], 4):
                    q[:, i] = -np.matmul(self.D(x[i], 4), np.matmul(e.B(x[i], 4), e.d))
                    T[i] = np.matmul(e.Nxy(x[i], 4), e.d)
                    break
        if type == "heat":
            flux_plot = plt
            flux_plot.plot(x, q[0, :], label=r"$q_x$")
            flux_plot.plot(x, q[1, :], label=r"$q_y$")
            flux_plot.grid()
            flux_plot.xlabel(r"x $(cm)$")
            flux_plot.ylabel(r"Heat flux $(W)$")
            flux_plot.legend(loc='best')
            flux_plot.title(r"Heat flux along line AB")
            flux_plot.show()

        elif type == "temp":
            e = self.Elements[3]
            T_plot = plt
            T_plot.plot(x[0:len(x)-2], T[0:len(x)-2], label=r"$T$")
            T_plot.plot([0,4,6], [T[0],np.matmul(e.Nxy(4, 4), e.d),T[-2]], linewidth=0, marker='s', color='b')
            T_plot.grid()
            T_plot.xlabel(r"x $(cm)$")
            T_plot.ylabel(r"Temperature $(C)$")
            T_plot.title(r"Temperature along line AB")
            T_plot.show()




