import numpy as np

class BilinearQuad:
    def __init__(self, x_nodes, y_nodes, g_node_numbers, el_num):
        self.n_nodes = 4
        self.edges = 4
        self.x = x_nodes
        self.y = y_nodes
        self.g_nodes = g_node_numbers
        self.el_num = el_num
        self.el_K = np.zeros([4, 4])
        self.el_Kt = np.zeros([4, 4])
        self.el_F = np.zeros(4).T
        self.el_F_bound = np.zeros(4).T
        self.el_F_bod = np.zeros(4).T
        self.d = np.zeros(4)

    def B(self, xi, eta):
        return np.matmul(np.linalg.inv(self.J_mat(xi, eta)), self.GN(xi, eta))

    def N(self, xi, eta):
        return (1/4)*np.array([(1 - eta) * (1 - xi),
                               (1 - eta) * (1 + xi),
                               (1 + eta) * (1 + xi),
                               (1 + eta) * (1 - xi)])

    def GN(self, xi, eta):
        return (1/4)*np.array([[-(1 - eta), (1 - eta), (1 + eta), -(1 + eta)],
                               [-(1 - xi), -(1 + xi), (1 + xi), (1 - xi)]])

    def J_mat(self, xi, eta):
        return np.matmul(self.GN(xi, eta), np.array([self.x, self.y]).T)

    def J(self, xi, eta):
        return np.linalg.det(self.J_mat(xi, eta))

    def in_element(self, x, y):
        """
        Probably (almost definitely) can't do this anymore
        :param x:
        :param y:
        :return:
        """
        # Must construct
        P = np.array([1, x, y])
        xi = np.matmul(P, np.linalg.inv(self.M))
        return np.sum(np.add(1 < xi, xi < 0)) == 0


    def display(self):
        print("Element number = ", self.el_num + 1, "\nx = ", self.x, "\ny = ", self.y, "\nGlobal nodes = ", self.g_nodes + 1)

    def quad_selector(self, n_gp):
        if n_gp == 1:
            xi = np.array([0])
            weights = np.array([2])
        elif n_gp == 2:
            xi = np.array([-1/(3**0.5), 1/(3**0.5)])
            weights = np.array([1, 1])
        elif n_gp == 3:
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        else:
            print("Invalid input for linear quadrature. Gauss points set to 3 for accuracy.")
            n_gp = 3
            xi = np.array([-(3/5)**0.5, 0, (3/5)**0.5])
            weights = np.array([5/9, 8/9, 5/9])
        return n_gp, xi, weights

    def iso_to_physical(self, nodes_pos, xi, eta):
        return np.dot(self.N(xi, eta), nodes_pos)

    def N_edge(self, edge, xi):
        if edge == 0:
            return self.N(xi, -1)
        elif edge == 1:
            return self.N(1, xi)
        elif edge == 2:
            return self.N(xi, 1)
        elif edge == 3:
            return self.N(-1, xi)

    def gauss(self, n_gp, integrand):
        n_gp, xi, weights = self.quad_selector(n_gp)
        eta = xi
        integrated = 0
        for i in range(n_gp):
            for j in range(n_gp):
                integrated += self.J(xi[i], eta[j]) * weights[i] * weights[j] * integrand(xi[i], eta[j])
        return integrated


