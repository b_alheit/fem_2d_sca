import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import cm
import numpy as np
import math

class Nonlinear_simulation:
    def __init__(self, x, y, ICA, u_bar, BCs, element, kt_int, u_guess, k_int, e, alpha=1, errorAnalysis=False):
        self.x = x
        self.y = y
        self.eSolved = e
        self.eLast = None
        self.eCurrent = None
        self.BCs = BCs
        self.n_nodes = len(x)
        self.ICA = ICA - 1
        self.n_el = len(ICA)
        self.K = np.zeros([self.n_nodes, self.n_nodes])
        self.Kt = np.zeros([self.n_nodes, self.n_nodes])
        self.d = np.zeros(self.n_nodes)
        self.G = np.zeros(self.n_nodes)
        self.converged = False
        self.diverged = False
        self.u_bar = u_bar
        self.divArray = np.zeros(4)
        self.alpha = alpha
        self.errorAnalysis = errorAnalysis
        if(self.errorAnalysis):
            self.Gn = []
            self.dn = []
            self.iterations = []

        self.kt_integrand = kt_int
        self.k_integrand = k_int
        self.u_guess = u_guess

        self.Elements = np.empty(self.n_el, dtype=element)
        for i in range(self.n_el):
            local_nodes = self.ICA[i]
            self.Elements[i] = element(x[local_nodes], y[local_nodes], local_nodes, i)

    def display_mesh(self):
        mesh_plot = plt
        for i in range(len(self.ICA)):
            el_nodes = self.ICA[i]
            el_x = self.x[el_nodes]
            el_y = self.y[el_nodes]
            cent_x = np.sum(el_x) / len(el_x)
            cent_y = np.sum(el_y) / len(el_y)
            mesh_plot.plot(np.append(el_x, el_x[0]), np.append(el_y, el_y[0]), color='black')
            mesh_plot.text(cent_x, cent_y, str(i+1), color='black')

        mesh_plot.scatter(self.x, self.y, color='black')

        for i in range(self.n_nodes):
            mesh_plot.text(self.x[i], self.y[i], str(i + 1), color='red', fontsize=10)

        mesh_plot.grid()
        mesh_plot.title("Mesh")
        mesh_plot.xlabel("x")
        mesh_plot.ylabel("y")
        mesh_plot.show()

    def assignGuess(self):
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.n_nodes):
                e.d[j] = self.u_guess(e.x[j], e.y[j])
                self.d[e.g_nodes[j]] = self.u_guess(e.x[j], e.y[j])

    def int_K(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                return self.k_integrand(xi, eta, e.B, e.N, e.d)

            e.el_K = e.gauss(n_gp, integrand)

    def int_Kt(self, n_gp):
        for i in range(self.n_el):
            e = self.Elements[i]

            def integrand(xi, eta):
                return self.kt_integrand(xi, eta, e.B, e.N, e.d)

            e.el_Kt = e.gauss(n_gp, integrand)

    def assemble(self):
        self.K = np.zeros([self.n_nodes, self.n_nodes])
        self.Kt = np.zeros([self.n_nodes, self.n_nodes])
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.n_nodes):
                self.K[e.g_nodes[j], e.g_nodes] += e.el_K[j, :]
                self.Kt[e.g_nodes[j], e.g_nodes] += e.el_Kt[j, :]

    def calc_G(self):
        self.G = np.matmul(self.K, self.d)

    def check_convergence(self):
        print(np.linalg.norm(self.G))
        self.eCurrent = np.linalg.norm(self.G)
        self.converged = self.eCurrent < self.eSolved

    def check_divergence(self):
        self.diverged = self.divArray[-1] > self.divArray[0]

    def applyBCs(self):
        for i in range(self.n_el):
            e = self.Elements[i]
            for j in range(e.n_nodes):
                if self.BCs[e.g_nodes[j]] == 1:
                    e.d[j] = self.u_guess(e.x[j], e.y[j])
                    self.d[e.g_nodes[j]] = self.u_guess(e.x[j], e.y[j])

        for i in range(self.n_nodes):
            if self.BCs[i] == 1:
                self.G[i] = 0
                self.Kt[i, :] = 0
                self.Kt[:, i] = 0
                self.Kt[i, i] = 1
                # self.K[i, :] = 0
                # self.K[:, i] = 0
                # self.K[i, i] = 1


    def solveNonlinear(self, n_gp):
        self.assignGuess()
        self.int_K(n_gp)
        self.int_Kt(n_gp)
        self.assemble()
        self.calc_G()
        self.applyBCs()

        it = 0
        while(not self.converged):
            print(it)
            # apply BCs?
            self.d -= self.alpha * np.matmul(np.linalg.inv(self.Kt), self.G)
            for i in range(self.n_el):
                self.Elements[i].d = self.d[self.ICA[i]]
            self.applyBCs()

            self.int_K(n_gp)
            self.int_Kt(n_gp)
            self.assemble()
            self.calc_G()
            self.applyBCs()
            self.check_convergence()
            if (self.errorAnalysis):
                self.Gn.append(self.eCurrent)
                self.dn.append(self.d.tolist())
                self.iterations.append(it + 1)
            if it > 3:
                self.divArray[0:3] = self.divArray[1:]
                self.divArray[-1] = self.eCurrent
                self.check_divergence()
            else:
                self.divArray[it] = self.eCurrent

            if self.diverged:
                print("Solution has diverged")
                break
            it+=1

        if self.errorAnalysis:
            self.dn = np.array(self.dn)
            self.iterations = np.array(self.iterations)

    def plot_surface(self, apprx_n_points, title):
        pp_iso = int(1 + math.ceil(math.sqrt(apprx_n_points / self.n_el)))
        xi_pos = np.linspace(-1, 1, pp_iso)
        eta_pos = xi_pos
        Xs = np.zeros([self.n_el, pp_iso, pp_iso])
        Ys = np.zeros([self.n_el, pp_iso, pp_iso])
        Zs = np.zeros([self.n_el, pp_iso, pp_iso])
        for i in range(self.n_el):
            X = np.zeros([pp_iso, pp_iso])
            Y = np.zeros([pp_iso, pp_iso])
            Z = np.zeros([pp_iso, pp_iso])
            e = self.Elements[i]
            for j in range(pp_iso):
                for k in range(pp_iso):
                    X[j, k] = e.iso_to_physical(e.x, xi_pos[j], eta_pos[k])

                    Y[j, k] = e.iso_to_physical(e.y, xi_pos[j], eta_pos[k])


                    Z[j, k] = np.matmul(e.N(xi_pos[j], eta_pos[k]), e.d)
            Xs[i] = X
            Ys[i] = Y
            Zs[i] = Z
        masked_array = np.ma.array(Zs, mask=np.isnan(Zs))
        cmap = cm.gnuplot
        cmap.set_bad('white', 1.)

        fig = plt.figure()
        ax = fig.gca(projection="3d")
        min_val, max_val = np.min(Zs), np.max(Zs)

        for i in range(self.n_el):
            ax.plot_surface(Xs[i], Ys[i], masked_array[i], vmin=min_val, vmax=max_val, cmap=cmap, linewidth=10,
                            antialiased=True)


        ax.set_zlim(min_val, max_val)
        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

        plt.show()