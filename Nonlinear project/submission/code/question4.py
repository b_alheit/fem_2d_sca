import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from nl_modules.Scalar_mesh import Nonlinear_simulation
from nl_modules.Scalar_elements import BilinearQuad as bq
matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


alphas = [1.0, 0.99, 0.9, 0.7, 0.4, 0.2]
markers = ['^', 'X', 's', 'v', 'o', 'x', 'P']

els = 14
A = 7
e = 1e-14
x_max = 1
x_min = 0
y_max = 1
y_min = 0

def simulation(alpha):
    def u_guess(x, y):
        return A*(0.25 - (x - 0.5) ** 2)


    def kt_int(xi, eta, Bfunc, Nfunc, dfunc):
        d = np.array([dfunc]).T
        B = Bfunc(xi, eta)
        N = Nfunc(xi, eta)

        scaVal = 1 + np.matmul(np.matmul(d.T, B.T), np.matmul(B, d))

        a1 = (scaVal ** (-1/2)) * np.matmul(B.T, B)
        a2 = (scaVal ** (-3/2)) * np.matmul(B.T, np.matmul(B, np.matmul(d, np.matmul(d.T, np.matmul(B.T, B)))))

        return a1 - a2

    def k_int(xi, eta, Bfunc, Nfunc, dfunc):
        d = np.array([dfunc]).T
        B = Bfunc(xi, eta)
        N = Nfunc(xi, eta)

        scaVal = 1 + np.matmul(np.matmul(d.T, B.T), np.matmul(B, d))
        return (scaVal ** (-1/2)) * np.matmul(B.T, B)


    def u_bar(x, y):
        if y == y_max or y == y_min or x == x_max or x == x_min:
            u = u_guess(x, y)
        else:
            u = None
        return u

    n = els + 1
    m = n

    nodes_x = np.zeros(m*n)
    nodes_y = np.zeros(m*n)

    x_vals = np.linspace(x_min, x_max, m)
    y_vals = np.linspace(y_min, y_max, n)

    BC = np.zeros(len(nodes_x))  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes

    for i in range(n*m):
        nodes_x[i] = x_vals[int(i/m)]
        nodes_y[i] = y_vals[i % n]

        if nodes_x[i] == x_min or nodes_x[i] == x_max or nodes_y[i] == y_min or nodes_y[i] == y_max:
            BC[i] = 1

    ICA = np.zeros([(m-1)*(n-1), 4])

    for i in range(len(ICA)):
        bl = 1 + i%(n-1) + n*int(i/(m-1))
        ICA[i, :] = [bl, bl+n, bl+n+1, bl+1]

    ICA = ICA.astype(int)

    quad_sim = Nonlinear_simulation(nodes_x, nodes_y, ICA, u_bar, BC, bq, kt_int, u_guess, k_int, e, alpha, True)
    quad_sim.solveNonlinear(3)
    return quad_sim


for i in range(len(alphas)):
    print("****** alpha = ", alphas[i], " **********")
    hexColour = str(hex(int(i*180/(len(alphas)))))[2:]
    if len(hexColour) == 1:
        hexColour = '0' + hexColour
    sim = simulation(alphas[i])
    grad = (np.log(sim.Gn[-1]) - np.log(sim.Gn[-2])) / (sim.iterations[-1] - sim.iterations[-2])
    grad1 = (np.log(sim.Gn[-1]) - np.log(sim.Gn[1])) / (np.log(sim.Gn[-2]) - np.log(sim.Gn[0]))
    print("grads \n", grad, "\n", grad1)
    plt.figure(1)
    plt.semilogy(sim.iterations, sim.Gn, color='black', marker=markers[i], label="$\\alpha$ = " + str(alphas[i]) + "Grad = " + str(grad)[:4])
    plt.figure(2)
    plt.loglog(sim.Gn[0:-1], sim.Gn[1:], color='#' + 3*hexColour, marker=markers[i],
                 label="$\\alpha$ = " + str(alphas[i]) + "\tGrad $\\approx$ " +str(grad1)[:4])

plt.figure(1)
plt.grid()
plt.xlabel("Iteration")
plt.ylabel("Euclidean norm of the residual")
plt.title("some stuff")
plt.legend()

plt.figure(2)
plt.grid()
plt.xlabel("$\sqrt{\mathbf{G}^{(n-1)} \cdot \mathbf{G}^{(n-1)}}$", fontsize=12)
plt.ylabel("$\sqrt{\mathbf{G}^{(n)} \cdot \mathbf{G}^{(n)}}$", fontsize=12)
plt.title("Comparison of the Euclidean norm of the residual at the current and previous iteration", fontsize=14, fontweight='bold')
plt.legend()

plt.show()
