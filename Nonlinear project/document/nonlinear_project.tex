\documentclass{article}

\author{Benjamin Alheit}
\date{\today}
\title{MEC5064Z Nonlinear Project}

\usepackage{graphicx}
\graphicspath{{figures/}}
\usepackage[margin = 0.8in]{geometry}
\usepackage{enumitem}
\usepackage{subcaption}
\usepackage{amsmath} 
\usepackage{amssymb} 
\usepackage{bm}
\usepackage{float}
\usepackage{subfig}
\usepackage[backend=biber]{biblatex}
\bibliography{ref.bib}
%\usepackage[nottoc]{tocbibind}


\begin{document}
\maketitle
\section{Derivations}
\subsection{Derivation of the weak form and its Galerkin discretization}
The strong form of the given minimal surface problem is
\begin{equation}
\begin{gathered}
\dfrac{\nabla^2 u}{\sqrt{1 + \left| \nabla u \right|^{2}}} = 0, \\
u = \overline{u} \qquad \bm{x} \in \Gamma_{D}, \\
\Gamma = \Gamma_D. 
\end{gathered}
\end{equation}
\\
To find the weak form of the problem, the governing equation is multiplied by an arbitrary weighting factor, $w$, of the same form as $u$ and the result is integrated over the domain
\begin{equation}
\int_{\Omega}\dfrac{w\nabla^2 u}{\sqrt{1 + \left| \nabla u \right|^{2}}} d\Omega = 0 \qquad \forall w.
\end{equation}
Green's theorem is then applied, which yields
\begin{equation}
\oint_\Gamma \dfrac{w}{\sqrt{1 + \left| \nabla u \right|^{2}}} \nabla u \cdot \bm{n} d\Gamma - \int_{\Omega} (\nabla w)^T (\nabla u) \dfrac{1}{\sqrt{1 + \left| \nabla u \right|^{2}}} d\Omega = 0 \qquad \forall w.
\end{equation}
A constraint is then placed on $w$ such that $w=0$ on $\Gamma_D$, and since the entire boundary is a Dirichlet boundary the Neumann condition need not be considered. Hence, the weak form of the problem is: \\
\\
\noindent Find $u \in H^1$ such that $u=\overline{u}$ on $\Gamma_D$ subject to the equation 
\begin{equation}
\int_{\Omega} (\nabla w)^T (\nabla u) \dfrac{1}{\sqrt{1 + \left| \nabla u \right|^{2}}} d\Omega = 0,
\label{eq:weak form}
\end{equation}
for all $w$, where $w\in H^1$ and $w=0$ on $\Gamma_D$. \\
\\
\noindent To find the Galerkin discretization, the following Galerkin approximations are made for $\nabla u$ and $\nabla w$
\begin{equation}
\nabla w = \bm{Bw}, \qquad \nabla u = \bm{Bd}.
\end{equation}
Substituting these approximations into the weak form yields the equation for the Galerkin discretization of the weak form
\begin{equation}
\begin{gathered}
\bm{w}^T\int_{\Omega} \bm{B^TB} \dfrac{1}{\sqrt{1 + (\bm{Bd})^T(\bm{Bd})}} d\Omega \bm{d} = 0,\\
\int_{\Omega} \bm{B^TB} (1 + \bm{d^TB^TBd})^{-1/2} d\Omega \bm{d} = 0.
\end{gathered}
\end{equation}
\subsection{Composing the residual problem}
Composing residual problem in the form $\bm{G}(\bm{d}) = \bm{R}(\bm{d}) - \bm{F}$ leads to 
\begin{equation}
\bm{G}(\bm{d}) = \int_{\Omega} \bm{B^TB} (1 + \bm{d^TB^TBd})^{-1/2} d\Omega \bm{d},
\label{eq:res}
\end{equation}
since there is no force term.

\subsection{Solving the problem utilizing the Newton-Raphson scheme}
Before detailing the steps to solve the problem, the tangent matrix will be derived. The residual problem is first cast into index notation
\begin{equation}
 G_{i}(d_{j}) = \int_{\Omega} B_{ki}B_{kj} (1 + d_{m}B_{lm}B_{ln}d_{n})^{-1/2} d\Omega d_{j}. 
\end{equation}
The tangent matrix is found by taking the derivative of the residual with respect to $d_j$
\begin{equation}
\begin{gathered}
\dfrac{\partial G_{i}(d_{j})}{\partial d_{j}} =\int_{\Omega} B_{ki}B_{kj} (1 + d_{m}B_{lm}B_{ln}d_{n})^{-1/2} d\Omega \\- \dfrac{1}{2}\int_{\Omega} B_{ki}B_{kj}B_{op}B_{oq}(1 + d_{m}B_{lm}B_{ln}d_{n})^{-3/2} d\Omega \left( d_{j}\dfrac{\partial d_{p}}{\partial d_{j}} d_{q} + d_{j}\dfrac{\partial d_{q}}{\partial d_{j}} d_{p} \right).
\end{gathered}
\label{eq:kt complex}
\end{equation}
Since
\begin{equation}
d_{j}\dfrac{\partial d_{q}}{\partial d_{j}} d_{p} + d_{j}\dfrac{\partial d_{p}}{\partial d_{j}} d_{q} = d_{j} \delta_{jq} d_{p} + d_{j}\delta_{jp} d_{q} = 2d_{q}d_{p},
\end{equation}
equation \ref{eq:kt complex} simplifies to
\begin{equation}
\begin{gathered}
\dfrac{\partial G_{i}(d_{j})}{\partial d_{j}} =\int_{\Omega} B_{ki}B_{kj} (1 + d_{m}B_{lm}B_{ln}d_{n})^{-1/2} d\Omega - \int_{\Omega} B_{ki}B_{kj}B_{op}B_{oq}(1 + d_{m}B_{lm}B_{ln}d_{n})^{-3/2} d\Omega d_qd_p.
\end{gathered}
\label{eq:kt index}
\end{equation}
Equation \ref{eq:kt index} is the tangent matrix in index form. It follows that the equation in matrix form is
\begin{equation}
\begin{gathered}
\dfrac{\partial \bm{G}(\bm{d})}{\partial \bm{d}} =\int_{\Omega} \bm{B^TB} (1 + \bm{d^TB^TBd})^{-1/2} d\Omega - \int_{\Omega} \bm{B^TB}\bm{B^TB} (1 + \bm{d^TB^TBd})^{-3/2} d\Omega \bm{dd^T}.
\end{gathered}
\label{eq:kt matrix}
\end{equation}
Hence, to solve the problem using the Newton-Raphson scheme, the following steps are taken:
\begin{enumerate}
\item An initial guess for the solution, $\bm{d}^{(0)}$, that satisfies the Dirichlet boundary conditions is made.
\item The residual at the $n^{th}$ iteration is calculated using $\bm{G}^{(n)}(\bm{d}^{(n)})$ as per equation \ref{eq:res}.
\item The tangent matrix, $\dfrac{\partial \bm{G}^{(n)}(\bm{d}^{(n)})}{\partial \bm{d}}$, is calculated using equation \ref{eq:kt matrix}.
\item The solution at the next iteration is then calculated using 
$$
\bm{d}^{(n+1)} = \bm{d}^{(n)} - \left[\dfrac{\partial \bm{G}^{(n)}(\bm{d}^{(n)})}{\partial \bm{d}}\right]^{-1}\bm{G}^{(n)}(\bm{d}^{(n)}).
$$
\item Steps 2-4 are then repeated until the change in the solution is negligible.
\end{enumerate}

\pagebreak


\section{Problem solving}
\subsection{Solving the boundary value problem \label{sec:first solve}}
The weak form for the problem is as shown in equation \ref{eq:weak form}, with the additional specifications for the boundary conditions:
\begin{equation}
\begin{gathered}
u(x,y) = A \left( \dfrac{1}{4} - \left(x-\dfrac{1}{2}\right)^2\right) \qquad \textrm{on} \quad \Gamma_{y=0},\Gamma_{y=1}\\
u(x,y) = 0 \qquad\quad\qquad\qquad\qquad\qquad \textrm{on} \quad \Gamma_{x=0},\Gamma_{x=1}
\end{gathered}
\end{equation}
Where $A$ is some scaling factor for the problem. The problem was solved using a Newton-Raphson iterative solving procedure along with the following parameters:
\begin{itemize}
\item The scaling factor, $A$, was set to $1$.
\item An initial guess for $u$ was chosen to be $u(x,y)=\left( \dfrac{1}{4} - \left(x-\dfrac{1}{2}\right)^2\right)$ over the entire domain.
\item A uniform mesh of $8\times 8$ Q1 elements was used.
\item The termination criteria was chosen to be when the Euclidean norm of the residual vector was less than $10^{-12}$ i.e $$\sqrt{\bm{G} \cdot \bm{G}} < 10^{-12}$$
\end{itemize}
The results of this solution are shown in figure \ref{fig:first solution}.
\begin{figure}[!hb]
\centering
\begin{subfigure}{0.45\textwidth}
\includegraphics[width=\linewidth]{q1l}
\end{subfigure}
\begin{subfigure}{0.45\textwidth}
\includegraphics[width=\linewidth]{q1r}
\end{subfigure}
\caption{Solution to the problem as defined in section \ref{sec:first solve}}
\label{fig:first solution}
\end{figure}
\subsection{Increased nonlinearity and mesh refinement}
To investigate the relationship between mesh refinement and the ability for a simulation to converge, the scaling factor $A$ was incremented for a simulation of a given number of elements per side until the simulation diverged\footnote{To check for divergence, the Euclidean norm of the residual (which will loosely be referred to as the `error' in this footnote) at each iteration was compared to the error 4 iterations prior to it. Initially the error was compared to the error one iteration prior to it. However, it was found that, occasionally, the error would increase slightly in the next iteration, but the solution would still eventually converge. Hence, the error was compared to the error of 4 iterations prior as a more robust method for checking for divergence.}. This value of $A$ was recorded and plotted against the number of elements per side that was used in the simulation as is shown in figure \ref{fig:aVn}.
\begin{figure}[!hb]
\centering 
\includegraphics[width = \linewidth]{nonlinearVconverge}
\caption{Plot of minimum value of $A$ that will cause a simulation to diverge against the number of elements that were used in that simulation}
\label{fig:aVn}
\end{figure} 
The minimum value of $A$ for which the solution will diverge remains constant at 9 for any number of elements used in the simulation (excluding the small perturbation at 8 and 9 elements per side). Hence, it is concluded that there is no correlation between the ability of a simulation to converge and the number of elements used in that simulation. Therefore, divergence cannot be avoided by increased mesh refinement. However, it can often be avoided by modifying the iterative solution method.

\subsection{Using dampening to improve convergence}
To improve the ability of a simulation to converge, a dampening factor, $\alpha$, was introduced such that the increment in the solution per iteration would be scaled by $\alpha$. Symbolically this is represented by
$$\bm{d}^{(n+1)} = \bm{d}^{(n)} + \alpha \bm{u}^{(n)}.$$
To investigate the relationship between the dampening factor and the ability of the simulation to converge, a range of values for $\alpha$ and $A$ were used to solve the problem on a mesh of $14\times 14$ elements. The maximum value of $\alpha$ that will lead to convergence for a given value $A$ was plotted and is shown in figure \ref{fig:alphaVA}.
\begin{figure}[!htb]
\centering 
\includegraphics[width = \linewidth]{alphaVA}
\caption{Plot of maximum value of $\alpha$ that will lead to convergence for a given value $A$}
\label{fig:alphaVA}
\end{figure} 
The figure shows that a dampening factor does indeed improve convergence. Furthermore, it shows that a smaller value of $\alpha$ is required for larger degrees of nonlinearity to lead to a converged solution.


\subsection{The relationship between the dampening factor and rate of convergence}
The error of a solution at some iteration $n$ is related to the error at the previous iteration by 
\begin{equation}
\epsilon^{(n)} = C\left(\epsilon^{(n-1)}\right)^k,
\end{equation}
where $\epsilon$ is the error, $C$ is some constant and $k$ is the rate of convergence. Taking the $\log$ of both sides of the equation yields
\begin{equation}
\log(\epsilon^{(n)}) = k\log(\epsilon^{(n-1)}) + \log(C).
\end{equation}
Hence, plotting some metric for the error at the current iteration, such as the Euclidean norm of the residual, against that metric for the error at the previous iteration on a log-log graph should produce a straight line. The rate of convergence of the solution method can then be inferred by finding the gradient of the line. This approach was used to determine how the dampening factor affects the rate of convergence of a damped Newton-Rahpson scheme. Simulations were run using a $14\times 14$ mesh, a value of $7$ for $A$ and various values of $\alpha$. The Euclidean norm of the residual at the current iteration was plotted against the Euclidean norm of the residual at the previous iteration on a log-log scale as shown in figure \ref{fig:gngn1}. Hence, the rate of convergence of the scheme was determined from the gradient of the graph.
\begin{figure}[!htb]
\centering
\includegraphics[width=\linewidth]{gngn1}
\caption{Log-log graph of the Euclidean norm of the residual at iteration $n$ against the Euclidean norm of the residual at iteration $n-1$.}
\label{fig:gngn1}
\end{figure}
The observed rate of convergence for an undamped Newton-Raphson scheme is $1.69$ which is lower than the theoretical convergence rate of $2$ \cite{emmaError}. This could be due to the fact that the undamped Newton-Raphson scheme solves the problem in so few iterations that the rate of convergence has not yet `stabilised' since, for all solution methods, the rate of convergence is not constant for the first couple of iterations. It is evident that the rate of convergence drops rapidly as soon as some dampening is introduced as is shown by the drop from $1.69$ to $1.13$ as $\alpha$ is changed from $1.0$ to $0.99$. From inspection, the rate of convergence continues to decrease as $\alpha$ decreases and tends to a value of $1.00$ as $\alpha$ tends to $0$.
\printbibliography

\end{document}
