import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import Scalar_mesh as Mesh
from Scalar_elements import BilinearQuad as bq

matplotlib.rc('text', usetex=True)
matplotlib.rcParams['text.latex.preamble']=[r"\usepackage{amsmath}"]


As =[]
Alphas = []

els = 14
e = 1e-12

Amax = 25

x_max = 1
x_min = 0
y_max = 1
y_min = 0
def simulation(A, alpha):
    def u_guess(x, y):
        return A*(0.25 - (x - 0.5) ** 2)


    def kt_int(xi, eta, Bfunc, Nfunc, dfunc):
        d = np.array([dfunc]).T
        B = Bfunc(xi, eta)
        N = Nfunc(xi, eta)

        scaVal = 1 + np.matmul(np.matmul(d.T, B.T), np.matmul(B, d))
        BTB = np.matmul(B.T, B)
        a1 = (scaVal ** (-1/2)) * np.matmul(B.T, B)

        a2 = (scaVal ** (-3/2)) * np.matmul(B.T, np.matmul(B, np.matmul(d, np.matmul(d.T, np.matmul(B.T, B)))))

        return a1 - a2
        # return (scaVal ** (-1/2)) * BTB - (scaVal ** -3/2) * np.matmul(np.matmul(BTB, np.matmul(d, d.T)), BTB)

    def k_int(xi, eta, Bfunc, Nfunc, dfunc):
        d = np.array([dfunc]).T
        B = Bfunc(xi, eta)
        N = Nfunc(xi, eta)

        scaVal = 1 + np.matmul(np.matmul(d.T, B.T), np.matmul(B, d))
        return (scaVal ** (-1/2)) * np.matmul(B.T, B)


    def u_bar(x, y):
        if y == y_max or y == y_min or x == x_max or x == x_min:
            u = u_guess(x, y)
        else:
            u = None
        return u


    n = els + 1
    m = n



    nodes_x = np.zeros(m*n)
    nodes_y = np.zeros(m*n)

    x_vals = np.linspace(x_min, x_max, m)
    y_vals = np.linspace(y_min, y_max, n)

    BC = np.zeros(len(nodes_x))  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes

    for i in range(n*m):
        nodes_x[i] = x_vals[int(i/m)]
        nodes_y[i] = y_vals[i % n]

        if nodes_x[i] == x_min or nodes_x[i] == x_max or nodes_y[i] == y_min or nodes_y[i] == y_max:
            BC[i] = 1


    ICA = np.zeros([(m-1)*(n-1), 4])

    for i in range(len(ICA)):
        a = i%(n-1)
        b = n*int((i+1)/m)
        bl = 1 + i%(n-1) + n*int(i/(m-1))
        ICA[i, :] = [bl, bl+n, bl+n+1, bl+1]

    ICA = ICA.astype(int)

    quad_sim = Mesh.Nonlinear_simulation(nodes_x, nodes_y, ICA, u_bar, BC, bq, kt_int, u_guess, k_int, e, alpha)
    quad_sim.solveNonlinear(3)
    return quad_sim

Alpha = 1/0.99
for i in range(8, Amax+1):
    print('******* Solving for A =', i, ' *******\n')
    diverged = True
    while(diverged):
        Alpha*=0.99
        print('*** Alpha= ', Alpha, ' ***')
        sim = simulation(i, Alpha)
        diverged = sim.diverged
    As.append(i)
    Alphas.append(Alpha)


plt.plot(As, Alphas, color='black', marker='^')

plt.xlabel('A', fontsize=12)
plt.ylabel('$\\alpha$', fontsize=12)
plt.title('Maximum value of $\\alpha$ for which the solution will converge \nusing a given scaling factor, $A$', fontsize=14, fontweight='bold')

plt.grid()

plt.show()