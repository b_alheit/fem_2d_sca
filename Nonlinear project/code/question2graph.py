import matplotlib.pyplot as plt

elements = [4, 5, 6, 7, 8, 9, 10]
A = [6, 21, 8, 8, 7, 9, 8]

plt.plot(elements, A, color='black', marker='^')

plt.xlabel('Elements per side')
plt.ylabel('A')
plt.title('Maximum value of A for which the solution will converge \nusing a given value of elements perside')

plt.grid()

plt.show()
