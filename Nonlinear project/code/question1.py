import numpy as np
import Scalar_mesh as Mesh
from Scalar_elements import BilinearQuad as bq

els = 8
A = 1
e = 1e-12
# e = 1
x_max = 1
x_min = 0
y_max = 1
y_min = 0

def u_guess(x, y):
    return A*(0.25 - (x - 0.5) ** 2)


def kt_int(xi, eta, Bfunc, Nfunc, dfunc):
    d = np.array([dfunc]).T
    B = Bfunc(xi, eta)
    N = Nfunc(xi, eta)

    scaVal = 1 + np.matmul(np.matmul(d.T, B.T), np.matmul(B, d))
    BTB = np.matmul(B.T, B)
    a1 = (scaVal ** (-1/2)) * np.matmul(B.T, B)

    a2 = (scaVal ** (-3/2)) * np.matmul(B.T, np.matmul(B, np.matmul(d, np.matmul(d.T, np.matmul(B.T, B)))))

    return a1 - a2
    # return (scaVal ** (-1/2)) * BTB - (scaVal ** -3/2) * np.matmul(np.matmul(BTB, np.matmul(d, d.T)), BTB)

def k_int(xi, eta, Bfunc, Nfunc, dfunc):
    d = np.array([dfunc]).T
    B = Bfunc(xi, eta)
    N = Nfunc(xi, eta)

    scaVal = 1 + np.matmul(np.matmul(d.T, B.T), np.matmul(B, d))
    return (scaVal ** (-1/2)) * np.matmul(B.T, B)


def u_bar(x, y):
    if y == y_max or y == y_min or x == x_max or x == x_min:
        u = u_guess(x, y)
    else:
        u = None
    return u


n = els + 1
m = n



nodes_x = np.zeros(m*n)
nodes_y = np.zeros(m*n)

x_vals = np.linspace(x_min, x_max, m)
y_vals = np.linspace(y_min, y_max, n)

BC = np.zeros(len(nodes_x))  # 1 denotes Dirichlet bc nodes, 0 denotes Neumann bc nodes

for i in range(n*m):
    nodes_x[i] = x_vals[int(i/m)]
    nodes_y[i] = y_vals[i % n]

    if nodes_x[i] == x_min or nodes_x[i] == x_max or nodes_y[i] == y_min or nodes_y[i] == y_max:
        BC[i] = 1


ICA = np.zeros([(m-1)*(n-1), 4])

for i in range(len(ICA)):
    a = i%(n-1)
    b = n*int((i+1)/m)
    bl = 1 + i%(n-1) + n*int(i/(m-1))
    ICA[i, :] = [bl, bl+n, bl+n+1, bl+1]

ICA = ICA.astype(int)

# quad_mesh = Mesh.Mesh(nodes_x, nodes_y, ICA, D, q_bar, u_bar, S, BC, bq, kt, u_guess, G, e)
quad_sim = Mesh.Nonlinear_simulation(nodes_x, nodes_y, ICA, u_bar, BC, bq, kt_int, u_guess, k_int, e)
quad_sim.solveNonlinear(3)
quad_sim.plot_surface(2000, "dasf")


# quad_mesh.solveNonlinear(3)

# quad_mesh = Mesh.Mesh(nodes_x, nodes_y, ICA, D, q_bar, u_bar, S, BC, bq, )
# quad_mesh.plot_surface(1000, "dasf")
#
# quad_mesh.display_mesh("mesh test")
#
# quad_mesh.int_F_bound(2)
# quad_mesh.int_F_bod(3)
# quad_mesh.int_K(3)
# quad_mesh.assemble()
# quad_mesh.solve_d()
#
# quad_mesh.plot_solution_element_wise(50000, "Bilinear Quadrilateral Solution", "cont")
